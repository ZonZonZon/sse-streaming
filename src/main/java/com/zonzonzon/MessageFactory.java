package com.zonzonzon;

import static java.util.Collections.emptySet;

import java.time.ZonedDateTime;

public class MessageFactory {

  public static Message createMessage(String input) {
    return new Message(
        input,
        0,
        "+17076204185",
        ZonedDateTime.now(),
        "MB0052a3791a1f455d8b6dd913bec3787d",
        "CH52735e1e5abf4f099d6aa50612d547da",
        "AC61620089e980a0470e4d3ea3f5c6df5b",
        "{\n"
            + "        \"delivered\": \"all\",\n"
            + "        \"read\": \"none\",\n"
            + "        \"undelivered\": \"none\",\n"
            + "        \"failed\": \"none\",\n"
            + "        \"total\": 1,\n"
            + "        \"sent\": \"all\"\n"
            + "      }",
        "https://conversations.twilio.com/v1/Conversations/CH52735e1e5abf4f099d6aa50612d547da/Messages/IMd510deedd299439ea5469885b7cb121f",
        ZonedDateTime.now(),
        "IMd510deedd299439ea5469885b7cb121f",
        emptySet());
  }
}
