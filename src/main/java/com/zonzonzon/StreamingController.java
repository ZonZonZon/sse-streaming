package com.zonzonzon;

import static com.zonzonzon.MessageFactory.createMessage;
import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

import java.time.Duration;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class StreamingController {

 public static final Queue<Message> messages = new ConcurrentLinkedQueue<>();

  @PostMapping(value = "/send-message")
  public ResponseEntity<HttpStatus> sendMessage(@RequestBody String input) {
    messages.add(createMessage(input));
    return ResponseEntity.of(Optional.of(ACCEPTED));
  }

  @GetMapping(value = "/read-messages", produces = TEXT_EVENT_STREAM_VALUE)
  public Flux<ServerSentEvent<Message>> streamEvents() {
    return Flux.interval(Duration.ofSeconds(5))
        .filter(interval -> !messages.isEmpty())
        .map(interval -> ServerSentEvent.<Message>builder().data(messages.poll()).build());
  }
}
