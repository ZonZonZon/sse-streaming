package com.zonzonzon;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

@EnableWebFlux
@Configuration
@RequiredArgsConstructor
public class StreamingRouter {

  /**
   * See http://www.stackstalk.com/2022/01/server-sent-events-with-spring-webflux.html
   */
  public final Sinks.Many<Message> messagesSink = Sinks.many().replay().all();

  @Bean
  public RouterFunction<ServerResponse> myRestRoutes() {
    return route()
        .POST("/add", contentType(TEXT_PLAIN), this::add)
        .GET("/subscribe", accept(TEXT_EVENT_STREAM), this::subscribe)
        .build();
  }

  public Mono<ServerResponse> add(ServerRequest serverRequest) {
    return serverRequest
        .bodyToMono(String.class)
        .map(MessageFactory::createMessage)
        .doOnNext(messagesSink::tryEmitNext)
        .flatMap(message -> ServerResponse.ok().contentType(APPLICATION_JSON).build())
        .log();
  }

  public Mono<ServerResponse> subscribe(ServerRequest serverRequest) {
    return ServerResponse.ok()
        .contentType(TEXT_EVENT_STREAM)
        .body(messagesSink.asFlux(), Message.class)
        .log();
  }
}
