package com.zonzonzon;

import java.time.ZonedDateTime;
import java.util.Set;

class Message {

  private final String body;
  private final int index;
  private final String author;
  private final ZonedDateTime dateUpdated;
  private final String participantSid;
  private final String conversationSid;
  private final String accountSid;
  private final String delivery;
  private final String url;
  private final ZonedDateTime dateCreated;
  private final String sid;
  private final Set<String> attributes;

  public Message(
      String body,
      int index,
      String author,
      ZonedDateTime dateUpdated,
      String participantSid,
      String conversationSid,
      String accountSid,
      String delivery,
      String url,
      ZonedDateTime dateCreated,
      String sid,
      Set<String> attributes) {

    this.body = body;
    this.index = index;
    this.author = author;
    this.dateUpdated = dateUpdated;
    this.participantSid = participantSid;
    this.conversationSid = conversationSid;
    this.accountSid = accountSid;
    this.delivery = delivery;
    this.url = url;
    this.dateCreated = dateCreated;
    this.sid = sid;
    this.attributes = attributes;
  }

  public String getBody() {
    return body;
  }

  public int getIndex() {
    return index;
  }

  public String getAuthor() {
    return author;
  }

  public ZonedDateTime getDateUpdated() {
    return dateUpdated;
  }

  public String getParticipantSid() {
    return participantSid;
  }

  public String getConversationSid() {
    return conversationSid;
  }

  public String getAccountSid() {
    return accountSid;
  }

  public String getDelivery() {
    return delivery;
  }

  public String getUrl() {
    return url;
  }

  public ZonedDateTime getDateCreated() {
    return dateCreated;
  }

  public String getSid() {
    return sid;
  }

  public Set<String> getAttributes() {
    return attributes;
  }
}
