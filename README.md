# JSON streaming with SSE

This pilot application shows how to open an infinite streaming connection (reactive with WebFlux)
and feed it asynchronously with events. Streaming is supported by HTTP5 clients with a header
Content-Type: text/event-stream

## Technologies

- Java 11
- Maven
- SpringBoot
- Webflux
- SSE

## The Test Plan

- Run the Application.java
- Open a browser at http://localhost:8080/read-messages
- Start adding messages with requests using resources/api/send-message.http